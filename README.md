# get-all-gitlab-projects

A script to get all your projects from gitlab, and fetches all the branches

## Prerequisites

You need jq:
```bash
sudo apt install jq
```

## Get a gitlab token

Go to gitlab, on your profile's settings, then choose access token and create a new token with api rights.
Then, create a token.sh executable, containing the token variable with your token.

```bash
echo "token=xxxxxxxxxxxx" > token.sh && chmod u+x token.sh || exit 1
```

## Ssh rights

You must have a ssh key to retrieve your repo's (private?) data. You may need to run the script once in a terminal to add the repo's ssh fingerprint to your known_hosts.

## Using a specific folder

You may export an environment variable COPY_GITLAB_DIR to use this folder instead of current folder.

```bash
export COPY_GITLAB_DIR=/home/docker/git-server/repos
```

## Using a specific user

Run with sudo and use -u user option:

```bash
sudo -u *_my_user_* -E ./update.sh
```

-E flag preserves environment variables

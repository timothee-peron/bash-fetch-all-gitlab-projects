#!/bin/sh

. ./token.sh

if [ -z $token ] ; then
  echo "Please set the token variable in the token.sh file!"
  exit 1
fi

# could do:
# curl "https://gitlab.com/api/v4/projects?owned=true&simple=true" -H "Accept: application/json" -H "Authorization: Bearer your-token" | jq .[].ssh_url_to_repo -rc | xargs -L 1 git clone
#curl=$(curl "https://gitlab.com/api/v4/projects?owned=true&simple=true" -H "Accept: application/json" -H "Authorization: Bearer $token")
#echo "Curl result: $curl"
#if [ ! -z $(echo $curl | grep "401 Unauthorized") ]; then
#  >&2 echo "Invalid token"
#  exit 1
#fi

projects=$(curl "https://gitlab.com/api/v4/projects?owned=true&simple=true" -H "Accept: application/json" -H "Authorization: Bearer $token" | jq .[].ssh_url_to_repo -rc || (>&2 echo "Error reaching gitlab or token expired!"))
echo "Projects=$projects"

if [ -z "$projects" ]; then
  >&2 echo "No project available"
  exit 1
fi

# extract project name from stdin:
# sed -r 's/git@.*\/(.*).git/\1/'

# https://stackoverflow.com/questions/10312521/how-to-fetch-all-git-branchesœ
# git branch -r | grep -v '\->' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done
# git fetch --all
# git pull --all

pwd=$(pwd)

# you can set an env variable COPY_GITLAB_DIR
if [ ! -z $COPY_GITLAB_DIR ]; then
  echo "Directory set to $COPY_GITLAB_DIR"
  pwd=$COPY_GITLAB_DIR
  mkdir -p $pwd
  cd $pwd
fi

echo "pwd = $(pwd)"
touch ./test || exit 1
echo $(whoami) > ./me  || exit 1

for project in $projects ; do
  folder=$(echo $project | sed -r 's/git@.*\/(.*).git/\1/')
  # '''
  echo "Project  $folder "
  echo "   - url $project"

  if [ ! -d ${folder} ]; then
    echo "   - first time, cloning..."
    git clone $project
  fi
  
  cd $folder

  echo "   - fetch"
  git fetch --all || failed="true"
  echo "   - branches"
  #remote branches,                   remove whitespaces   , remove head   , remove local branches   : trim space: remove * (current br): prepend origin/ for filter     | track the branch (for branch tst: --track tst remote/tst)
  (git branch -r || failed="true" ) | sed -e 's/^[ \t]*//' | grep -v '\->' | grep -Fvx "$(git branch | tr -d '*' | sed -e 's/^[ \t]*//' | awk '{print "origin/" $0}' -)" | while read remote; do git branch  --track "${remote#origin/}" "$remote"; done
  echo "   - pull"
  git pull --all --rebase  || failed="true"

  cd $pwd
done

if [ ! -z "$failed" ]; then
  >&2 echo "An error has occured"
  exit 1
fi

